function customerFaultReport() {

    var customerForm = {};
    customerForm.faultType = $("#customer_fault_types").val();
    customerForm.stationFrom = $("#customer_station_from").val();
    customerForm.stationTo = $("#customer_station_to").val();
    customerForm.boardingTime = $("#customer_boarding_time").val();
    customerForm.coach = $("#customer_train_coach").val();
    customerForm.locationOnCoach = $("#customer_coach_location").val();
    customerForm.faultDescription = $("#customer_fault_description").val();

    if (!customerForm.faultType ||
        !customerForm.stationFrom ||
        !customerForm.stationTo ||
        !customerForm.boardingTime ||
        !customerForm.coach ||
        !customerForm.locationOnCoach)
    {
        //alert("Fill in mandatory fields!");
    }
    else {
        window.location.href = 'customer_fault_review.html';

        setObject('customerForm', customerForm);
    }
}

function getCustomerInformation(){
    var storedData = getObject("customerForm")

    $("#confirmation").html(
        "<style type='text/css'>"+
".tg  {border-collapse:collapse;border-spacing:0;}"+
".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}"+
".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px"+ "5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}"+
".tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}"+
".tg .tg-4c44{font-size:15px;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:center;vertical-align:top}"+
"</style>"+
"<table class='tg' align='center'>"+
  "<tr>"+
   "<th class='tg-4c44'>Fault Type:</th>"+
    "<th class='tg-c3ow'>" + storedData.faultType + "</th>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Station From:</td>"+
    "<td class='tg-c3ow'>" + storedData.stationFrom + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Station To:</td>"+
    "<td class='tg-c3ow'>" + storedData.stationTo + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Boarding Time:</td>"+
    "<td class='tg-c3ow'>" + storedData.boardingTime + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Coach:</td>"+
    "<td class='tg-c3ow'>" + storedData.coach + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Location on Coach:</td>"+
    "<td class='tg-c3ow'>" + storedData.locationOnCoach + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Fault Description :</td>"+
    "<td class='tg-c3ow'>" + storedData.faultDescription + "</td>"+
 "</tr>"+
"</table>"
    );
}

function customerFaultSubmit(){
    window.location.href = 'customer_confirmation.html';
}

// populate fields with whats stored within the session storage
function customer_repopulate() {
    if (sessionStorage.getItem('customerForm') != null)
    {
        var storedData = JSON.parse(sessionStorage.getItem('customerForm'));

        $("#customer_fault_types").val(storedData.faultType);
        $("#customer_station_from").val(storedData.stationFrom);
        $("#customer_station_to").val(storedData.stationTo);
        $("#customer_train_coach").val(storedData.coach);
        $("#customer_coach_location").val(storedData.locationOnCoach);
        $("#customer_boarding_time").val(storedData.boardingTime);
        $("#customer_fault_description").val(storedData.faultDescription);
    }
}