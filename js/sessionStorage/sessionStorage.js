function setObject(key, value) {
// puts an object into session storage
    window.sessionStorage.setItem(key, JSON.stringify(value));
};

function getObject(key) {
// gets an object from session storage
    var storage = window.sessionStorage;
    var value = storage.getItem(key);
    return value && JSON.parse(value);
};
