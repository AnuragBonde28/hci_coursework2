function backToHome() {
    window.location.href = 'index.html';
}

function backToReportNewFault(){
    window.location.href = 'report_new_fault.html';
}

function backToStaffReport() {
    window.location.href = 'staff_fault_report.html';
}

function backToCustomerReport(){
    window.location.href = 'customer_fault_report.html';
}

function previousFaults(){
    window.location.href = 'see_reported_faults.html';
}

function backToStaffNumber(){
    window.location.href = 'staff_employee_number.html';
}

function viewRaisedFaults(){
    if(sessionStorage.getItem('customerForm')){
        var storedData = getObject("customerForm");

        $("#previousFaults").html(
            "<style type='text/css'>"+
".tg  {border-collapse:collapse;border-spacing:0;}"+
".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}"+
".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px"+ "5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}"+
".tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}"+
".tg .tg-4c44{font-size:15px;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:center;vertical-align:top}"+
"</style>"+
"<table class='tg' align='center'>"+
  "<tr>"+
   "<th class='tg-4c44'>Fault Type:</th>"+
    "<th class='tg-c3ow'>" + storedData.faultType + "</th>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Station From:</td>"+
    "<td class='tg-c3ow'>" + storedData.stationFrom + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Station To:</td>"+
    "<td class='tg-c3ow'>" + storedData.stationTo + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Boarding Time:</td>"+
    "<td class='tg-c3ow'>" + storedData.boardingTime + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Coach:</td>"+
    "<td class='tg-c3ow'>" + storedData.coach + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Location on Coach:</td>"+
    "<td class='tg-c3ow'>" + storedData.locationOnCoach + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Fault Description :</td>"+
    "<td class='tg-c3ow'>" + storedData.faultDescription + "</td>"+
 "</tr>"+
"</table>"
            //"<div>Fault Type: " + storedData.faultType + "</div>" +
            //"<div>Station From: " + storedData.stationFrom + "</div>" +
            //"<div>Station To: " + storedData.stationTo + "</div>" +
            //"<div>Coach: " + storedData.coach + "</div>" +
            //"<div>Location on Coach: " + storedData.locationOnCoach + "</div>" +
            //"<div>Staff Boarding Time : " + storedData.boardingTime + "</div>" +
            //"<div>Fault Description : " + storedData.faultDescription + "</div>"
        );
    }
    else if(sessionStorage.getItem('staffForm')){
        var storedData = getObject("staffForm");
        var storedEmployeeNumber = getObject("employeeNumberCheck");
        $("#previousFaults").html(
            "<style type='text/css'>"+
".tg  {border-collapse:collapse;border-spacing:0;}"+
".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}"+
".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px"+ "5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}"+
".tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}"+
".tg .tg-4c44{font-size:15px;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:center;vertical-align:top}"+
"</style>"+
"<table class='tg' align='center'>"+
"<tr>"+
   "<th class='tg-4c44'>Staff Number:</th>"+
    "<th class='tg-c3ow'>" + storedEmployeeNumber.staffNumber + "</th>"+
  "</tr>"+
  "<tr>"+
   "<th class='tg-4c44'>Fault Type:</th>"+
    "<th class='tg-c3ow'>" + storedData.faultType + "</th>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Station From:</td>"+
    "<td class='tg-c3ow'>" + storedData.stationFrom + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Station To:</td>"+
    "<td class='tg-c3ow'>" + storedData.stationTo + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Boarding Time:</td>"+
    "<td class='tg-c3ow'>" + storedData.boardingTime + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Coach:</td>"+
    "<td class='tg-c3ow'>" + storedData.coach + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Location on Coach:</td>"+
    "<td class='tg-c3ow'>" + storedData.locationOnCoach + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Fault Description :</td>"+
    "<td class='tg-c3ow'>" + storedData.faultDescription + "</td>"+
 "</tr>"+
"</table>"
            //"<div>Staff Number: " + storedEmployeeNumber.staffNumber + "</div>" +
            //"<div>Fault Type: " + storedData.faultType + "</div>" +
            //"<div>Station From: " + storedData.stationFrom + "</div>" +
            //"<div>Station To: " + storedData.stationTo + "</div>" +
            //"<div>Coach: " + storedData.coach + "</div>" +
            //"<div>Location on Coach: " + storedData.locationOnCoach + "</div>" +
            //"<div>Staff Boarding Time : " + storedData.boardingTime + "</div>" +
            //"<div>Fault Description : " + storedData.faultDescription + "</div>"
        );
    }
}