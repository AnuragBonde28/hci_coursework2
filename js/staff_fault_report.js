// staff number check function
function staffNumber(){

    var employeeNumberCheck = {};
    employeeNumberCheck.staffNumber = $("#staff_number").val();

    console.log(employeeNumberCheck.staffNumber);

    if(employeeNumberCheck.staffNumber == 1000){
        setObject('employeeNumberCheck', employeeNumberCheck);
        window.location.href = 'staff_fault_report.html';
    }

    else if(!employeeNumberCheck.staffNumber){
        document.getElementById('label_error').innerHTML = 'Please enter your employee number!';
        //alert("Please enter your employee number!");
        location.reload();
        return false;
    }

    else if(employeeNumberCheck.staffNumber !== 1000){
        document.getElementById('label_error').innerHTML = 'Invalid staff number!';
        //alert("Invalid staff number!");
        location.reload();
        return false;
    }
}

function staffFaultReport() {

    var staffForm = {};
    staffForm.faultType = $("#fault_types").val();
    staffForm.stationFrom = $("#station_from").val();
    staffForm.stationTo = $("#station_to").val();
    staffForm.boardingTime = $("#boarding_time").val();
    staffForm.coach = $("#train_coach").val();
    staffForm.locationOnCoach = $("#coach_location").val();
    staffForm.faultDescription = $("#fault_description").val();

    if (!staffForm.faultType ||
        !staffForm.stationFrom ||
        !staffForm.stationTo ||
        !staffForm.boardingTime ||
        !staffForm.coach ||
        !staffForm.locationOnCoach) {
        //alert("Fill in mandatory fields!");
    }
    else {
        window.location.href = 'staff_fault_review.html';
        // store input to the session storage

        setObject('staffForm', staffForm);
    }
}

function getStaffInformation(){

    var storedData = getObject("staffForm");
    var storedEmployeeNumber = getObject("employeeNumberCheck");

    $("#confirmation").html
    (
        "<style type='text/css'>"+
".tg  {border-collapse:collapse;border-spacing:0;}"+
".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}"+
".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px"+ "5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}"+
".tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}"+
".tg .tg-4c44{font-size:15px;font-family:Arial, Helvetica, sans-serif !important;;border-color:inherit;text-align:center;vertical-align:top}"+
"</style>"+
"<table class='tg' align='center'>"+
"<tr>"+
   "<th class='tg-4c44'>Staff Number:</th>"+
    "<th class='tg-c3ow'>" + storedEmployeeNumber.staffNumber + "</th>"+
  "</tr>"+
  "<tr>"+
   "<th class='tg-4c44'>Fault Type:</th>"+
    "<th class='tg-c3ow'>" + storedData.faultType + "</th>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Station From:</td>"+
    "<td class='tg-c3ow'>" + storedData.stationFrom + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Station To:</td>"+
    "<td class='tg-c3ow'>" + storedData.stationTo + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Boarding Time:</td>"+
    "<td class='tg-c3ow'>" + storedData.boardingTime + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Coach:</td>"+
    "<td class='tg-c3ow'>" + storedData.coach + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Location on Coach:</td>"+
    "<td class='tg-c3ow'>" + storedData.locationOnCoach + "</td>"+
  "</tr>"+
  "<tr>"+
    "<td class='tg-4c44'>Fault Description :</td>"+
    "<td class='tg-c3ow'>" + storedData.faultDescription + "</td>"+
 "</tr>"+
"</table>"
    );
}

function staffFaultSubmit(){
    window.location.href = 'staff_confirmation.html';
}

// populate fields with whats stored within the session storage
function repopulateFaultReport() {
    if (sessionStorage.getItem('staffForm') !=null)
    {
        var storedData = JSON.parse(sessionStorage.getItem('staffForm'));

        $("#fault_types").val(storedData.faultType);
        $("#station_from").val(storedData.stationFrom);
        $("#station_to").val(storedData.stationTo);
        $("#train_coach").val(storedData.coach);
        $("#coach_location").val(storedData.locationOnCoach);
        $("#boarding_time").val(storedData.boardingTime);
        $("#fault_description").val(storedData.faultDescription);
    }
}

function repopulateStaffNumber() {
    if (sessionStorage.getItem('employeeNumberCheck') != null)
    {
        var storedEmployeeNumber = JSON.parse(sessionStorage.getItem('employeeNumberCheck'));
        $("#staff_number").val(storedEmployeeNumber.staffNumber);
    }
}
